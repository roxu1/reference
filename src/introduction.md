# Introduction

Roxu is a program aimed at facilitating language bindings by
providing a standardised subset of C alongside a json header which
can be used to automatically generate bindings for a wide range
of modern, high-level languages.

## Goals

 - *Simplicity* - Roxu should have a very small core reference
   so that new languages can be implemented quickly and easily.
   This also helps reduce bugs.
 - *Ease of use* - Roxu should be easy enough that it's headers
   can be generated automatically by a binary, and then translated
   automatically by another program - thus reducing programmer 
   effort.

## Non-Goals

 - *Full C Compatibility* - C is a very low level language, and
   many C idioms are illogical in other, non-C based, languages.
   Thus having good C support should not be a priority.
