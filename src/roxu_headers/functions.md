# Functions

 - Functions are represented by an object with the following
   properties:
     - `introduced_generics`
     - `introduced_lifetimes`
     - `args`: an `$array` containing `$ident_types`
     - `return_type`: a `$type` that this function may return
     - `hints`: `$hints`
 - Functions can have the following hints:
     - `const` - The function is constant and takes no arguments
     - `variadic` - The last element in the function should be an array-like
       object, which should be treated as a variadic element (i.e. in C printf)
