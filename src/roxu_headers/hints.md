# Hints

 - In Roxu, hints act as a way of indicating how some object is to
   be treated. For example, a `$function` with the `const` hint
   indicates that the function in languages with support should
   be treated as a constant.
 - So we define the `$hints` array - an array of string for hints.
 - Critically, hints don't *have* to be implemented for each backend.
   For example, the rust backend will choose to ignore the `variadic`
   hint as rust itself lacks variadics.
