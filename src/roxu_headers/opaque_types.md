# Opaque Types

 - All non-primitive types in Roxu are opaque - thus allowing them to
   be reasonably understood by many different programming languages.
 - The `$property` type is used to represent a property and has properties:
     - `mut` - is this property mutable
     - `type` - the `$type` of this property
 - The `$opaque_type` is used for defining opaque-types and has properties:
     - `introduced_generics`
         - It is to be noticed that with the introduction of these generics,
           there is an important distinction to be made. These allow for
           properties to be of certain generic types. To use this type in the
           `$type` to use `$generics` to apply these types. It is considered
           invalid if the `$generics` field is not complete.
     - `properties` - a `$map` of `$property`s
