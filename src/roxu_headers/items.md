# The `$items` and `$item` objects

 - The `$items` object is a `$map` of `$string`s to `$item`s
 - The `$item` object is an externally tagged enum with the
   following variants:
     - `module`: `$module`
     - `function`: `$function`
     - `opaque_type`: `$opaque_type`
     - `trait`: `$trait`
