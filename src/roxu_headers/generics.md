# Generics And Lifetimes

## Generics

 - Generics are types that implement traits, e.g. type `T` implementing trait
   `U`.
 - They are represented in json by using the `$type` object with
   a `$generic` mode.
 - They can be introduced into scope by several objects, and in
   which case act in different ways
     - This is done through `introduced_generics` - `$map` of `$string`s 
       to `$array`s, where the array contains the `$trait_types` of
       the object.
 - When generics are applied to a type they are done so via the
   `$generics` map: which maps `$string`s to `$type`s.
 - Whereas, when generics are used by themselves they are used
   through the `$generic` variant of `$type`

## Lifetimes

 - Lifetimes act as 'locks.' That is, if a function returns a
   object with lifetimes, then whichever argument had the same
   lifetime, cannot be used until the return time has been dropped.
 - In headers they are represented by the `$lifetimes` array:
   i.e. an array of strings to in scope lifetimes. Certain objects
   can introduce lifetimes into scopes, e.g. functions.
 - In headers they are introduced through the `$introduced_lifetimes`
   string array
