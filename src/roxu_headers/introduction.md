# Roxu Headers

 - Roxu headers a defined in a file named `roxu.json5`
 - This file is in the [json5](json5.org) format.
 - At the top of this file are the following properties:
     - `roxu_version`: The semver version of roxu this file was written for
     - `name`: The name of the project - a string
     - `items`: An `$items`
     - `impls`: An array of `$implementation` that this project
       introduces.
