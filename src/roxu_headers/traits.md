# Traits And Implementations

 - In Roxu, traits allow for generics. A generic type is, on its own,
   pretty useless if it does not have some form of traits. Traits are equivilent
   to mixins and interfaces
 - C, the only ubiquitous FFI language, does not support generics or traits of
   any type. This forces traits to not be compile time (in the present)
 - An `$associated_item` is an externally tagged enum with the following
   variants:
     - `function`: a `$function`
 - The `$trait` object is an object with the following properties:
     - `introduced_generics`
     - `required_items`: a map of `$associated_item`s that must be implemented
         - **TODO: Rework this to support typedefs**
     - `optional_items` a map of `$associated_item`s that may be implemented on
       a type implementing this trait - elsewise falling back to some form
       of default implementation.
