# Types

 - A `$path` is an `$array` of `$string`s to the object is indicates
 - When represented in JSON, the `$type` is an externally enum
   with two variants:
     - `generic` - An object with the following properties:
         - `generic`: the in scope generic to use
         - `lifetimes`: the lifetimes to be applied 
     - `absolute` - A object with the following properties:
         - `path`: a `$path` to an type (absolute path)
         - `generics`: `$generics` to be applied to the type
         - `lifetimes`: `$lifetimes` to be applied to the type
 - Likewise, for referencing traits, the `$trait_type` object is
   used, with properties:
     - `path`: a `$path` to a trait (absolute path)
     - `generics`: `$generics` to be applied to the type
 - Also, an `$ident_type` is a json object containing:
     - `name`: the `$string` name of this,
     - `type`: the `$type` of this,
