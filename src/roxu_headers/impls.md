# Implementations

 - In Roxu, to associate functions with an object you use an implementation,
   both for items specific to that object, and for trait implementations
 - This is defined through the following type, `$impl` with properties:
     - `introduced_generics` - Any generics introduced into the implementation,
       primary for use in specifying who this implementation is for,
     - `on` - A `$type` to specifically implement this on this type
     - `trait` - The trait that is being implemented. This can either be a
       `$trait_type` or `null` - but should only be null in the case of `on` being
       an absolute `$type`.
     - `associated_items` - the `$associated_item`s to be implemented on this.
       This will be merged with any `$required_items` of the `trait` if not
       null
