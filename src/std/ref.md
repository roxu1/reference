# Reference Types

 - The `ref` module contains the following types:
     - `ref<'a, T>` - an immutable pointer
     - `refmut<'a, T>` - a mutable, but onowned reference
     - `refowned<T>` - an owned box
