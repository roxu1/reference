# The Standard Library

Roxu has a standard library (of sorts) with the following modules

 - `traits` - A set of compatibility traits for common things
   such as comparison, numerical operations, indexing, strings, &c
   &c.
 - `primitives` - Machine primitives
 - `llt` - Low level types
 - `ref` - Reference types
