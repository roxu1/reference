# Low Level Types
 
 - The `llt` module contains the following low-level types:
     - `usize` - pointer-sized unsigned integer,
     - `isize` - pointer-sized signed integer,
     - `ptr<T>` - a pointer to type `T`.
