# Primitives

 - The primitives module contains machine primitives:
     - `u8`, `u16`, `u32`, `u64` and `u128` - unsigned integer types
     - `i8`, `i16`, `i32`, `i64` and `i128` - signed integer types
     - `f32`, `f64` - float types
     - `bool` - a boolean type which can only be `0` or `1`,
     and stores itself in a single byte.
     - `void` - a void type, with the size of a `u8`. All `void`s should be
       `0x00` so they are all equal.
