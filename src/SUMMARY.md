# Summary

- [Introduction](./introduction.md)

---

# Roxu Headers

- [Roxu Headers](./roxu_headers/introduction.md)
- [The `$items` and `$item` objects](./roxu_headers/items.md)
- [Modules](./roxu_headers/modules.md)
- [Types](./roxu_headers/types.md)
- [Functions](./roxu_headers/functions.md)
- [Opaque Types](./roxu_headers/opaque_types.md)
- [Traits & Implementations](./roxu_headers/traits.md)
- [Generics & Lifetimes](./roxu_headers/generics.md)
- [Implementations](./roxu_headers/impls.md)
- [Hints](./roxu_headers/hints.md)

---

# Standard Library

- [The Standard Library](./std/introduction.md)
- [Primitives](./std/primitives.md)
- [Low Level Types](./std/llt.md)
- [Reference Types](./std/ref.md)

---

# Representations

- [Representations](./repr/introduction.md)
- [C](./repr/c.md)
    - [Roxu Headers](./repr/c/introduction.md)
    - [The `$items` and `$item` objects](./repr/c/items.md)
    - [Modules](./repr/c/modules.md)
    - [Types](./repr/c/types.md)
    - [Functions](./repr/c/functions.md)
    - [Opaque Types](./repr/c/opaque_types.md)
    - [Traits & Implementations](./repr/c/traits.md)
    - [Generics & Lifetimes](./repr/c/generics.md)
    - [Implementations](./repr/c/impls.md)
    - [Hints](./repr/c/hints.md)
