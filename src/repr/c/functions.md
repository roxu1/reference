# Functions

 - `introduced_generics` are introduced as specified in "Generics & Lifetimes"
 - `args`: Are represented as the args of the function
 - `return_type`: Is represented as the return type of the function in C
 - `hints`: All hints are IGNORED - including `variadic` and the like. This is
   so that incomplete representations for other languages do not get confused
   with variadics, thus simplifying Roxu
