# The `$items` and `$item` objects

 - `$items`: written as each individual element would be, with the
   name being added to the contextual path.
 - `$item`: written as each variant would be
