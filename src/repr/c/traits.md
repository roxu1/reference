# Traits And Implementations

 - The `$trait` object is represented as follows:
     - `introduced_generics` as specified in "Generics & Lifetimes"
     - A struct is created with the path of the trait, with the fields being
       `required_items` and `optional_items` (each individually ordered by string
       value as is implemented in Rust) function pointers. `optional_items` may
       be `null`.
     - Then there are functions to corresponding to each of the aformentioned
       items, with signatures as follows:

       ```c
       /// Here ... represents the rest of the arguments
       ${item_return_type} ${trait_name}__${item_name}(${trait_name} * self, ...)
       ```

       This function will always run even if an optional item is `null`
