# Roxu Headers

 - A context is stored, as the item tree is traversed, containing information
   about:
     - In-scope generics
     - In-scope lifetimes
     - The current path
 - The toplevel Roxu Header is translated as follows:
     - `roxu_version` - parameter is ignored
     - `name` - added as the first element in the contextual module path
     - `items` - serialised as specified in its dedicated chapter
     - `impls` - serialsied as specified in its dedicated chapter
