# Implementations

 - The arguments and return types to all functions are kept constant.
 - This is the prefix:
     - Which becomes `${on*}__${?trait*}`
     - Where `*` indicates that all generics should be expanded such that the
       generics of `Foo<T = Bar, U = Quz>` becomes
       `Foo__o__T__e__Bar__a__U__e__Quz__c__`.
         - Here `__e__` indicates either an `=`
         - Here `__o__` indicates `<`
         - Here `__c__` indicates `>`
         - Here `__a__` indicates `,`
         - This is a bad system
         - Within each layer the strings are sorted by their byte values
         - This allows for the horrible: `A<T = B<T = C, U = D>, U = E>` to
           become `A__o__T__e__B__o__T__e__C__a__U__e__D__c__a__U__e__E__c__`
         - This is why C is not a first-class language in Roxu.
     - `?` indicates that the trait is not required
 - If the `trait` is `null` then the `$impl` is represented as a series of
   functions with the prefix used to prefix their name (followed by a `__`, of
   course)
 - Otherwise, a function with just the name of the prefix is created which
   takes one argument, `self`, of type `roxu__generic` (even if its not a generic
   type), and returns the trait object for the implementation.
