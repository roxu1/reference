# Opaque Types

 - A typedef is created between the path of the type and `void`. This ensures
   their use solely behind types in `roxu::ref` - thus allowing for lifetime
   guaruntees to hold
 - `$opaque_type` is written by:
     - `introduced_generics`: as specified in "Generics & Lifetimes"
     - `properties`:
         - Each property is serialised as the following functions:
             ```c
             // Gets an immutable reference to this property (that is its
             // really:
             //
             // ref<'a, ${property_type}> ...(ref<'a, $obj> self);
             ${property_type} * ${obj_path}__${property_name}__get($obj_path const * const self);
             
             // Gets a mutable reference to this property
             // This is only written if `mut` is `true`
             ${property_type} * ${obj_path}__${property_name}__get_mut($obj_path * self);
             ```
