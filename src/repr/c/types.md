# Types

 - `$path`: written as:
     - All the segments joined by `__` (double underscore `_` characters)
     - Any segment containing characters that are invalid in C should be
      represented by having the invalid character replaced with `Uxxxx` where
      `xxxx` is the hexadecimal representation of its unicode code point.
 - When represented in C, the `$type` object varies based of its variant:
     - `$generic` - the generic becomes a `roxu__generic`.
        See "Generics & Lifetimes" for more information.
     - `$absolute` - is written as the `$path` would be. Generics and lifetimes
       are to be ignored.
- The exceptions are `ref` types which are serialised as follows:
    - `ref<T, 'a>` as `const * const T`
    - `refmut<T, 'a` as `* T`
    - `refowned<T>` as however `Drop<T>` would be serialised.
