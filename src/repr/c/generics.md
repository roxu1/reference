# Generics And Lifetimes

## Generics

 - Generics are introduced into scope by the `introduced_generics`, thus mapping
   them to a struct which looks as follows:

   ```c
   struct roxu__generic {
       // The self object - i.e. a ref/refmut<'a, T>
       void * self;
       // This is all the generics of the object, in the order they are in the
       // are in the `introduced_generics` array. That is, this field contains
       // pointers to the trait objects relevant to self. It is constructed
       // through some magic.
       void * generics;
   }
   ```

## Lifetimes

**Lifetimes are ignored**. That is, it is the programmers responsibility to read
the documentation and understand the lifetime. This is because C lacks
underlying lifetime technology and the C implementation should have as minimal
an implementation as possible to make it easiest to build implementations atop.
