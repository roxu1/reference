# Representations

Obviously Roxu cannot function on just headers - they have to
be programmatically represented in some form or another. This segment of the
book aims to explain how different languages will be represented in Roxu, and
should aim to mirror the structure of the
[Roxu Header](../roxu_headers/introduction.md) section as much as possible to
make it easy to follow.
