# C

For Roxu to function, C is the primary representation; that is since it is the
only ubiquitous FFI language all calls in Roxu go through a C interface.
Therefore, new representations must be able to work with the C interface in
order to call Roxu functions.
